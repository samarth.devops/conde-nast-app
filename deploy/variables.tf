variable "prefix" {
  default = "appt"
}

variable "availability_zone_a" {

  type        = string
  default     = "us-east-2a"
  description = "av z a"

}

variable "availability_zone_b" {

  type        = string
  default     = "us-east-2b"
  description = "avzb"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "bastion-key"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "097532237264.dkr.ecr.us-east-2.amazonaws.com/django-hw-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "097532237264.dkr.ecr.us-east-2.amazonaws.com/hello-world-app:latest"
}

