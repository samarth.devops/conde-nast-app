terraform {

  backend "s3" {
    bucket         = "terraform-backend-demo"
    key            = "app.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "terraform-demo"
  }
}


# Configure the AWS Provider
provider "aws" {
  version = "~> 3.0"
  region  = "us-east-2"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
}



